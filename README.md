### 基于 Creator 3.x 内置渲染管线的 Shader 示例

------

- ##### 卡通水体渲染

    ![](READMEIMG/ToonWater.gif)

- ##### sprite / plane 翻页

    ![](READMEIMG/TurnPage.gif)

- ##### 2d 颜色调整

    ![](READMEIMG/ColorAdjust2D.gif)

- ##### 内置材质（渲染优先级 / 模板缓冲 / 深度测试 / 图元填充）

    ![](READMEIMG/BuiltinMaterial.gif)
