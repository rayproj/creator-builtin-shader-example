import { _decorator, Component, Node, find, MeshRenderer, Sprite, SpriteFrame, tween, Tween } from 'cc';
import { PageRender } from './Page/PageRender';
const { ccclass, property, executeInEditMode } = _decorator;

@ccclass('BookMain')
@executeInEditMode
export class BookMain extends Component {
    start() {
        this.scheduleOnce(() => {
            const rt = PageRender.screenshot(find("Canvas/rt0"));
            find("Canvas/book").getComponent(MeshRenderer).material.setProperty("mainTex", rt);
        }, 1);
    }
}

