
import { _decorator, Component, Node, Material, v4, RenderTexture, screen } from "cc";
const { ccclass, property } = _decorator;

@ccclass("CameraDirectionUpdate")
export class CameraDirectionUpdate extends Component {
    @property(Material)
    waterMaterial: Material;
    @property(RenderTexture)
    depthRT: RenderTexture;

    onLoad() {
        const winSize = screen.windowSize;
        this.depthRT.resize(winSize.width, winSize.height);

        this.updateCamDir();
    }

    update() {
        this.updateCamDir();
    }

    private updateCamDir() {
        const forward = this.node.forward;
        this.waterMaterial.setProperty("cameraDirection", v4(forward.x, forward.y, forward.z, 0.0));
    }

}
