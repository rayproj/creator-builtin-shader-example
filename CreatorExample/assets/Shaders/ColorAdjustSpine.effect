// Copyright (c) 2017-2020 Xiamen Yaji Software Co., Ltd.
CCEffect %{
  techniques:
  - passes:
    - vert: sprite-vs:vert
      frag: sprite-fs:frag
      depthStencilState:
        depthTest: false
        depthWrite: false
      blendState:
        targets:
        - blend: true
          blendSrc: src_alpha
          blendDst: one_minus_src_alpha
          blendDstAlpha: one_minus_src_alpha
      rasterizerState:
        cullMode: none
      properties:
        alphaThreshold: { value: 0.5 }
        brightness: { value: 1.0, editor: { slide: true, range: [0, 2.0], step: 0.001 } }
        saturation: { value: 1.0, editor: { slide: true, range: [0, 2.0], step: 0.001 } }
        contrast: { value: 1.0, editor: { slide: true, range: [0, 1.5], step: 0.001 } }
}%

CCProgram sprite-vs %{
  precision highp float;
  #include <cc-global>
  #if USE_LOCAL
    #include <cc-local>
  #endif

  in vec3 a_position;
  in vec2 a_texCoord;
  in vec4 a_color;

  out vec4 v_light;
  out vec2 uv0;

  #if TWO_COLORED
    in vec4 a_color2;
    out vec4 v_dark;
  #endif

  vec4 vert () {
    vec4 pos = vec4(a_position, 1);

    #if USE_LOCAL
      pos = cc_matWorld * pos;
    #endif

    pos = cc_matViewProj * pos;

    uv0 = a_texCoord;

    v_light = a_color;
    #if TWO_COLORED
      v_dark = a_color2;
    #endif

    return pos;
  }
}%

CCProgram sprite-fs %{
  precision highp float;
  #include <alpha-test>

  uniform Constant{
    float brightness;
    float saturation;
    float contrast;
  };

  in vec4 v_light;
  #if TWO_COLORED
    in vec4 v_dark;
  #endif
  in vec2 uv0;
  #pragma builtin(local)
  layout(set = 2, binding = 11) uniform sampler2D cc_spriteTexture;

  vec4 frag () {

    vec4 o = vec4(1, 1, 1, 1);
    #if TWO_COLORED
      vec4 texColor = vec4(1, 1, 1, 1);

      texColor *= texture(cc_spriteTexture, uv0);
 	    o.a = texColor.a * v_light.a;
      o.rgb = ((texColor.a - 1.0) * v_dark.a + 1.0 - texColor.rgb) * v_dark.rgb + texColor.rgb * v_light.rgb;
    #else
      o *= texture(cc_spriteTexture, uv0);
      o *= v_light;
    #endif

    vec3 finalColor = o.rgb * brightness;
    float gray1 = 0.2125 * o.r + 0.7154 * o.g + 0.0721 * o.b;
    vec3 grayColor = vec3(gray1, gray1, gray1);
    finalColor = mix(grayColor, finalColor, saturation);
    vec3 avgColor = vec3(0.5, 0.5, 0.5);
    finalColor = mix(avgColor, finalColor, contrast);
    o.rgb = finalColor.rgb;

    ALPHA_TEST(o);
    return o;
  }
}%
