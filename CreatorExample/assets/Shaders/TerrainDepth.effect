// Copyright (c) 2017-2020 Xiamen Yaji Software Co., Ltd.
CCEffect %{
  techniques:
  - name: opaque
    passes:
    - vert: terrain-vs
      frag: terrain-fs:depth_32bits
      properties: &props
        UVScale:      { value: [1, 1, 1, 1] }
        lightMapUVParam: { value: [0, 0, 0, 0] }
        metallic:     { value: [0, 0, 0, 0] }
        roughness:    { value: [1, 1, 1, 1] }
        weightMap:    { value: black }
        detailMap0:   { value: grey }
        detailMap1:   { value: grey }
        detailMap2:   { value: grey }
        detailMap3:   { value: grey }
        normalMap0:   { value: normal }
        normalMap1:   { value: normal }
        normalMap2:   { value: normal }
        normalMap3:   { value: normal }
        lightMap:     { value: grey }
}%

CCProgram terrain-vs %{
  precision mediump float;
  #include <cc-global>
  #include <cc-local>
  #include <cc-fog-vs>
  #include <cc-shadow-map-vs>

  in vec3 a_position;
  in vec3 a_normal;
  in vec2 a_texCoord;

  out highp vec3 v_position;
  out mediump vec3 v_normal;
  #if USE_NORMALMAP
    out mediump vec3 v_tangent;
    out mediump vec3 v_binormal;
  #endif
  out mediump vec2 uvw;
  out mediump vec2 uv0;
  out mediump vec2 uv1;
  out mediump vec2 uv2;
  out mediump vec2 uv3;
  out mediump vec3 luv;
  out mediump vec3 diffuse;

  out vec4 v_screenPos;

  uniform TexCoords {
    vec4 UVScale;
    vec4 lightMapUVParam;
  };

  void main () {
    vec3 worldPos;
    worldPos.x = cc_matWorld[3][0] + a_position.x;
    worldPos.y = cc_matWorld[3][1] + a_position.y;
    worldPos.z = cc_matWorld[3][2] + a_position.z;

    vec4 pos = vec4(worldPos, 1.0);
    pos = cc_matViewProj * pos;

    uvw = a_texCoord;
    uv0 = a_position.xz * UVScale.x;
    uv1 = a_position.xz * UVScale.y;
    uv2 = a_position.xz * UVScale.z;
    uv3 = a_position.xz * UVScale.w;
    #if CC_USE_LIGHTMAP
      luv.xy = lightMapUVParam.xy + a_texCoord * lightMapUVParam.zw;
      luv.z = lightMapUVParam.z;
    #endif

    v_position = worldPos;
    v_normal = a_normal;
    CC_TRANSFER_FOG(vec4(worldPos, 1.0));

    #if USE_NORMALMAP
      v_tangent = vec3(1.0, 0.0, 0.0);
      v_binormal = vec3(0.0, 0.0, 1.0);

      v_binormal = cross(v_tangent, a_normal);
      v_tangent = cross(a_normal, v_binormal);
    #endif

    CC_TRANSFER_SHADOW(vec4(worldPos, 1.0));

    v_screenPos = pos;
    gl_Position = pos;
  }
}%

CCProgram terrain-fs %{
  precision highp float;
  #include <output>
  #include <packing>

  in highp vec3 v_position;
  in mediump vec3 v_normal;
  #if USE_NORMALMAP
    in mediump vec3 v_tangent;
    in mediump vec3 v_binormal;
  #endif
  in mediump vec2 uvw;
  in mediump vec2 uv0;
  in mediump vec2 uv1;
  in mediump vec2 uv2;
  in mediump vec2 uv3;
  in mediump vec3 diffuse;
  in mediump vec3 luv;

  in vec4 v_screenPos;

  uniform PbrParams {
    vec4 metallic;
    vec4 roughness;
  };

  uniform sampler2D weightMap;
  uniform sampler2D detailMap0;
  uniform sampler2D detailMap1;
  uniform sampler2D detailMap2;
  uniform sampler2D detailMap3;
  uniform sampler2D normalMap0;
  uniform sampler2D normalMap1;
  uniform sampler2D normalMap2;
  uniform sampler2D normalMap3;
  uniform sampler2D lightMap;

  vec4 depth_32bits () {
    float depth = v_screenPos.z / v_screenPos.w * 0.5 + 0.5;
    vec4 col = packDepthToRGBA(depth);
    return CCFragOutput(col);
  }
}%
